/*
 * Copyright 2017-2016 Munish Thakur
 *
 * Licensed under the Apache License, Version 2.0 (the "License");
 * you may not use this file except in compliance with the License.
 * You may obtain a copy of the License at
 *
 * http://www.apache.org/licenses/LICENSE-2.0
 *
 * Unless required by applicable law or agreed to in writing, software
 * distributed under the License is distributed on an "AS IS" BASIS,
 * WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
 * See the License for the specific language governing permissions and
 * limitations under the License.
 */
package motobeans.com.productshowcase.extensions

import android.content.Context
import android.widget.Toast
import java.util.regex.Pattern

/**
 * Kotlin Extensions for String
 */

/**
 * Show Toast of String
 * @param context Context?
 */
fun String.Ex_showToast(context: Context?){
  Toast.makeText(context, this, Toast.LENGTH_LONG).show()
}

/**
 * Check if String is Empty
 * @return Boolean
 */
fun String.Ex_isNotEmptyOrNullOrBlank() : Boolean{
  return !this.isNullOrBlank() && !this.isNullOrEmpty()
}

/**
 * Check if Email matches Valid pattern "^(.+)@(.+)$"
 * @return Boolean
 */
fun String.isValidEmail(): Boolean {
  val regex = "^(.+)@(.+)$"

  val pattern = Pattern.compile(regex)

  val matcher = pattern.matcher(this)
  return matcher.matches()
}

/**
 * Check if String is Integer
 * @return Boolean
 */
fun String.isInteger(): Boolean {
  val regex = "^[0-9]*$"
  val pattern = Pattern.compile(regex)

  val matcher = pattern.matcher(this)
  return matcher.matches()
}

/**
 * Convert String to Integer
 * @return Int
 */
fun String.Ex_toInt(): Int {
  var value : Int = 0
  try {
    value = Integer.parseInt(this)
  } catch (e : Exception){

  }
  return value
}