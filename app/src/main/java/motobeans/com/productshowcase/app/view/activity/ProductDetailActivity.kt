/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.app.view.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AlertDialog
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView.LayoutManager
import com.bumptech.glide.Glide
import motobeans.com.productshowcase.R
import motobeans.com.productshowcase.app.viewmodel.Injection
import motobeans.com.productshowcase.app.viewmodel.ProductViewModel
import motobeans.com.productshowcase.app.viewmodel.ViewModelFactory
import motobeans.com.productshowcase.app.persistence.model.Product
import motobeans.com.productshowcase.app.persistence.model.Store
import motobeans.com.productshowcase.app.view.adapters.recycler.ColorOptionRecycler
import motobeans.com.productshowcase.app.view.adapters.recycler.StoreRecycler
import motobeans.com.productshowcase.databinding.ActivityProductDetailBinding

/**
 * It shows Product Detail with all necessary detail shown on Screen
 * It fetched data from Database and populate view elements on Screen
 *
 * View: All product related views
 */
class ProductDetailActivity : AppCompatActivity() {

  /**
   * Static start activity method which is starting point
   * Created so single point of entry will be there in whole project.
   */
  companion object {

    val KEY_ID = "id"

    fun start(context: Context, id: Int? = null) {
      val bundle = Bundle()
      if (id != null) let { bundle.putInt(KEY_ID, id) }

      val intent = Intent(context, ProductDetailActivity::class.java)
      intent.putExtras(bundle)
      context.startActivity(intent)
    }
  }

  private lateinit var binding: ActivityProductDetailBinding
  private lateinit var viewModelFactory: ViewModelFactory
  private lateinit var productViewModel: ProductViewModel

  /**
   * Entry point of Activity
   */
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = DataBindingUtil.setContentView(this, R.layout.activity_product_detail)

    init()
  }

  private var product: Product? = null
  private val id: Int? by lazy { intent?.extras?.getInt(KEY_ID) }

  /**
   * It handles variable initialization, Setting click listeners and attaching view model to Activity
   */
  private fun init() {
    viewModelFactory = Injection.provideViewModelFactory(this)

    // Initialize Product View Model
    productViewModel = ViewModelProviders.of(this, viewModelFactory).get(
        ProductViewModel::class.java)

    /**
     * If id is null then return else proceed by fetching related data from DB
     */
    if (id != null) {
      // Observe changes in Product
      productViewModel.getProduct(id.toString()).observe(this,
          Observer { product -> populateTask(product) })
    } else {
      return
    }

    // Attach onBackPress event with Back button on Toolbar
    binding.toolbar.ivToolbarBack.setOnClickListener { finish() }
  }

  /**
   * Populate Product UI
   * @param product Product?
   */
  private fun populateTask(product: Product?) {
    this.product = product

    binding.toolbar.tvToolbarTitle.text = product?.name

    binding.tvProductName.text = product?.name
    binding.tvProductDescription.text = product?.description
    binding.tvRegularPrice.text = product?.regular_price.toString()
    binding.tvSalePrice.text = product?.sale_price.toString()

    populateColorOptions()
    populateStores()

    // Edit Product Listener
    binding.buttonEditProduct.setOnClickListener { _ -> AddUpdateProductActivity.start(this, id) }

    // Delete Product Listener
    binding.buttonDeleteProduct.setOnClickListener { _ -> deleteProduct(id) }

    // Load Product Image using Glide
    Glide.with(this).load(product?.product_photo).into(binding.ivProductImage)

    // Click listener on Product Image - Show full screen Image
    binding.ivProductImage.setOnClickListener { FullScreenImageActivity.start(this, product?.product_photo) }
  }

  /**
   * Populate color Listing using recyclerAdapter
   */
  private fun populateColorOptions() {
    val layoutManager: LayoutManager = LinearLayoutManager(this, LinearLayoutManager.HORIZONTAL,
        false)

    val adapter = ColorOptionRecycler(this, product?.colors)
    binding.rvProductColor.layoutManager = layoutManager
    binding.rvProductColor.adapter = adapter
    binding.rvProductColor.setHasFixedSize(true)
    binding.rvProductColor.isNestedScrollingEnabled = false
  }

  /**
   * Populate Store Listing using recyclerAdapter
   */
  private fun populateStores() {
    val layoutManager: LayoutManager = LinearLayoutManager(this)

    val stores = ArrayList<Store>()
    for (index in product?.stores?.indices ?: 0..0) {
      for (storeMap in product?.stores?.get(index)?.keys ?: 0..0) {
        stores.add(product?.stores?.get(index)?.get(storeMap) ?: Store(1, "", "", ""))
      }
    }
    val adapter = StoreRecycler(this, stores)
    binding.rvProductStores.layoutManager = layoutManager
    binding.rvProductStores.adapter = adapter
    binding.rvProductStores.setHasFixedSize(true)
    binding.rvProductStores.isNestedScrollingEnabled = false
  }


  /**
   * Show Confirmation dialog if User really wants to Delete Product
   * Yes : Delete Product from DB and finish Screen
   * No : Dismiss Dialog
   */
  private fun deleteProduct(productId: Int?) {
    val builder = AlertDialog.Builder(this, R.style.AppCompatAlertDialogStyle)
    builder.setTitle("Delete!")
    builder.setMessage("Do you really want to Delete this product ?")
    builder.setPositiveButton(android.R.string.yes, { dialog, _ ->
      productViewModel.deleteProductById(productId!!)
      dialog.dismiss()
      finish()
    })
    builder.setNegativeButton(android.R.string.cancel, { dialog, _ ->
      dialog.dismiss()
    })
    val dialog = builder.create()

    dialog.show()
  }
}
