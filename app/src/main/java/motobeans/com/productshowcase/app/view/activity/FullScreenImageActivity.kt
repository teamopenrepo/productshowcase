/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.app.view.activity

import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import com.bumptech.glide.Glide
import motobeans.com.productshowcase.R
import motobeans.com.productshowcase.databinding.ActivityFullScreenImageBinding
import motobeans.com.productshowcase.databinding.ActivityProductDetailBinding

/**
 * It shows Product Detail with all necessary detail shown on Screen
 * It fetched data from Database and populate view elements on Screen
 *
 * View: All product related views
 */
class FullScreenImageActivity : AppCompatActivity() {

  /**
   * Static start activity method which is starting point
   * Created so single point of entry will be there in whole project.
   */
  companion object {

    val KEY_IMAGE_URL = "imageURL"

    fun start(context: Context, imageURL: String? = null) {
      val bundle = Bundle()
      if (imageURL != null) let { bundle.putString(KEY_IMAGE_URL, imageURL) }

      val intent = Intent(context, FullScreenImageActivity::class.java)
      intent.putExtras(bundle)
      context.startActivity(intent)
    }
  }

  private lateinit var binding: ActivityFullScreenImageBinding
  private val imageURL: String? by lazy { intent?.extras?.getString(KEY_IMAGE_URL) }

  /**
   * Entry point of Activity
   */
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = DataBindingUtil.setContentView(this, R.layout.activity_full_screen_image)

    init()
  }


  /**
   * It handles variable initialization, Setting click listeners to Activity
   */
  private fun init() {

    binding.toolbar.tvToolbarTitle.text = "Product Image"

    /**
     * If imageURL is null then return else proceed by loading image to View
     */
    if (imageURL != null) {
      Glide.with(this).load(imageURL).into(binding.ivProductImage)
    } else {
      return
    }

    // Attach onBackPress event with Back button on Toolbar
    binding.toolbar.ivToolbarBack.setOnClickListener { finish() }
  }
}
