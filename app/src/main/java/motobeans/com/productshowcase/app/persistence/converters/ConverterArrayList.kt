/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.app.persistence.converters

import android.arch.persistence.room.TypeConverter
import com.google.gson.Gson
import com.google.gson.reflect.TypeToken


/**
 * The 'ConverterArrayList' is converting ArrayList to String and vice-versa to make it store-able entity in Database
 */
class ConverterArrayList {

  /**
   * Returns a {@code ArrayList<String>} that is parsed
   * from JSON String using {@link #GSON}
   *
   * @param value
   *            data string
   * @return A {@code ArrayList<String>}
   *
   * @see GSON
   * @see ArrayList
   */
  @TypeConverter
  fun fromString(value: String): ArrayList<String> {
    val listType = object : TypeToken<ArrayList<String>>() {}.type
    return Gson().fromJson(value, listType)
  }

  /**
   * Returns a {@code String} that is converted
   * from @param ArrayList<String> using {@link GSON}
   *
   * @param value
   *            ArrayList<String>
   * @return A {@code ArrayList<String>}
   *
   * @see GSON
   * @see ArrayList
   */
  @TypeConverter
  fun fromArrayLisr(list: ArrayList<String>): String {
    val gson = Gson()
    return gson.toJson(list)
  }

}