/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.app.persistence.db

import android.arch.persistence.room.Database
import android.arch.persistence.room.Room
import android.arch.persistence.room.RoomDatabase
import android.content.Context
import motobeans.com.productshowcase.app.persistence.dao.ProductDao
import motobeans.com.productshowcase.app.persistence.model.Product
import motobeans.com.productshowcase.app.persistence.model.Store

/**
 * The Room database.
 */
@Database(entities = [Product::class, Store::class], version = 1)
abstract class ProductDB : RoomDatabase() {

  @SuppressWarnings("WeakerAccess")
  abstract fun productDao(): ProductDao

  companion object {
    /** The only instance */
    @Volatile private var INSTANCE: ProductDB? = null

    /**
     * Gets the singleton instance of ProductDB.
     *
     * @param context The context.
     * @return The singleton instance of ProductDB.
     */
    fun getInstance(context: Context): ProductDB =
        INSTANCE ?: synchronized(this) {
          INSTANCE ?: buildDatabase(context).also { INSTANCE = it }
        }

    /**
     * Switches the internal implementation with an empty in-memory database.
     *
     * @param context The context.
     */
    private fun buildDatabase(context: Context) =
        Room.databaseBuilder(context.applicationContext,
            ProductDB::class.java, "product.db")
            .build()
  }
}