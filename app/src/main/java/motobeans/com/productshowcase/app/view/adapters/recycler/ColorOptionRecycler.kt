/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.app.view.adapters.recycler

import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import motobeans.com.productshowcase.app.view.adapters.holders.ColorOptionHolder
import motobeans.com.productshowcase.R
import motobeans.com.productshowcase.databinding.ItemColorBinding

/**
 * RecyclerAdapter to populate color options
 * @param activity AppCompatActivity
 * @param items List<String> where String is Hex code of color
 */
class ColorOptionRecycler(activity: AppCompatActivity, items: List<String>?) : RecyclerView.Adapter<ColorOptionHolder>() {
  val context: Context = activity

  // List of colors
  val items: List<String>? = items

  /**
   * Get Layout Inflator
   */
  private fun getLayoutInflater(parent: ViewGroup?): LayoutInflater {
    return LayoutInflater.from(parent?.context)
  }

  override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ColorOptionHolder {
    val binding: ItemColorBinding = DataBindingUtil.inflate(getLayoutInflater(parent), R.layout.item_color, parent, false)
    return ColorOptionHolder(context = context, binding = binding)
  }

  override fun getItemCount(): Int {
    return items?.size ?: 0
  }

  override fun onBindViewHolder(holder: ColorOptionHolder?, position: Int) {
    holder?.handleCard(position, items!!.get(position))
  }
}