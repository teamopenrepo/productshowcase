/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.app.viewmodel

import android.arch.lifecycle.ViewModel
import android.arch.lifecycle.ViewModelProvider
import motobeans.com.productshowcase.app.persistence.dao.ProductDao

/**
 * Factory to wrap each ViewModel with Data access object of Product Entity
 *
 * @constructor dataSource: ProductDao
 */
class ViewModelFactory(private val dataSource: ProductDao) : ViewModelProvider.Factory {

  /**
   * Get DAO of Product Entity
   *
   * @return ProductDao
   */
  fun getDBSource() = dataSource

  /**
   * Wrap DAO of Product Entity with Provided Model class T
   *
   * @param modelClass Model class of Generic type <T>
   * @return T
   */
  override fun <T : ViewModel> create(modelClass: Class<T>): T {
    if (modelClass.isAssignableFrom(ProductViewModel::class.java)) {
      return ProductViewModel(dataSource) as T
    }
    throw IllegalArgumentException("Unknown ViewModel class")
  }
}
