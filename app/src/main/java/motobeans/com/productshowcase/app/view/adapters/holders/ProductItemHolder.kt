/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.app.view.adapters.holders

import android.content.Context
import android.support.v7.widget.RecyclerView
import com.bumptech.glide.Glide
import motobeans.com.productshowcase.app.view.activity.ProductDetailActivity
import motobeans.com.productshowcase.app.persistence.model.Product
import motobeans.com.productshowcase.databinding.ItemProductBinding

/**
 * RecyclerAdapter's Holder to populate Product view
 * @param context Context
 * @param binding ItemProductBinding
 */
class ProductItemHolder(val context: Context, val binding: ItemProductBinding) : RecyclerView.ViewHolder(binding.root) {

  /**
   * Handles view of Product Item
   * It accepts Product Entity and attach to the view
   *
   * @param position Int (position of view in RecyclerView)
   * @param itemData Product
   */
  fun handleCard(position: Int, itemData: Product) {
    binding.tvProductName.text = itemData.name
    binding.tvProductDescription.text = itemData.description
    binding.tvRegularPrice.text = itemData.regular_price.toString()
    binding.tvSalePrice.text = itemData.sale_price.toString()

    // Load Image of Product
    Glide.with(context).load(itemData.product_photo).into(binding.ivProductImage)

    // List Item Click Listener - Starts Product Detail screen
    binding.root.setOnClickListener { _ -> ProductDetailActivity.start(context, itemData.id)
    }
  }
}