/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.app.persistence.dao

import android.arch.lifecycle.LiveData
import android.arch.persistence.room.Dao
import android.arch.persistence.room.Delete
import android.arch.persistence.room.Insert
import android.arch.persistence.room.OnConflictStrategy
import android.arch.persistence.room.Query
import motobeans.com.productshowcase.app.persistence.model.Product

/**
 * Created by munishkumarthakur on 09/12/17.
 */
@Dao
interface ProductDao {

  /**
   * Get all task stored in DB.
   *
   * @return all products <LiveData<List<Product>?>> from the table
   */
  @Query("SELECT * FROM Product")
  fun getAllProducts() : LiveData<List<Product>?>

  /**
   * Get a product by id.
   *
   * @param id String
   * @return LiveData<Product> the product from the table with a specific id.
   */
  @Query("SELECT * FROM Product WHERE id = :id LIMIT 1")
  fun getProductbyId(id: String) : LiveData<Product>


  /**
   * Insert a product in the database. If the product already exists, replace it.
   * @param product the product to be inserted.
   */
  @Insert(onConflict = OnConflictStrategy.REPLACE)
  fun insertProduct(product: Product)

  /**
   * Insert products in the database. If the product already exists, replace it.
   *
   * @param products the list of products to be inserted.
   */
  @Insert(onConflict = OnConflictStrategy.REPLACE)
  fun insertProducts(product: ArrayList<Product>)


  /**
   * Get count of rows in Product table
   *
   * @return LiveData<Int> - Where Int is count of table rows
   */
  @Query("Select COUNT(id) from Product")
  fun getProductCount() : LiveData<Int>

  /**
   * Delete product from table with specific id.
   *
   * @param id the id needs to be deleted from product table
   */
  @Query("DELETE FROM Product WHERE id = :id")
  fun deleteProductById(id: Int)

  /**
   * Delete product from table via object
   *
   * @param product the object needs to be deleted from product table
   */
  @Delete
  fun deleteProduct(task: Product)

  /**
   * Delete all products.
   */
  @Query("DELETE FROM Product")
  fun deleteAllProducts()
}