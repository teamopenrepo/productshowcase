/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.app.view.activity

import android.R.layout
import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.content.Context
import android.content.Intent
import android.databinding.DataBindingUtil
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.view.View
import android.widget.AdapterView
import android.widget.ArrayAdapter
import com.bumptech.glide.Glide
import motobeans.com.productshowcase.R
import motobeans.com.productshowcase.app.persistence.model.Product
import motobeans.com.productshowcase.app.persistence.model.Store
import motobeans.com.productshowcase.app.view.activity.AddUpdateProductActivity.UPDATE_TYPE.ADD
import motobeans.com.productshowcase.app.view.activity.AddUpdateProductActivity.UPDATE_TYPE.UPDATE
import motobeans.com.productshowcase.app.viewmodel.Injection
import motobeans.com.productshowcase.app.viewmodel.ProductViewModel
import motobeans.com.productshowcase.app.viewmodel.ViewModelFactory
import motobeans.com.productshowcase.application.ArchitectureApp
import motobeans.com.productshowcase.databinding.ActivityAddUpdateProductBinding
import motobeans.com.productshowcase.development.interfaces.FormValidation
import motobeans.com.productshowcase.extensions.Ex_toInt
import java.util.Random
import javax.inject.Inject

/**
 * Handle Add new product or Update existing product.
 * It handles both Database connection and Network call
 *
 * View: Auto-fill data of Product if Exist
 *
 * Validation: Validate form once user add or update Product
 */
class AddUpdateProductActivity : AppCompatActivity(), AdapterView.OnItemSelectedListener {

  /** ENUM type of Add or Update */
  enum class UPDATE_TYPE {
    ADD, UPDATE
  }

  /**
   * Static start activity method which is starting point
   * Created so single point of entry will be there in whole project.
   */
  companion object {

    val KEY_ID = "id"

    fun start(context: Context, id: Int? = 0) {
      val bundle = Bundle()
      id?.let { bundle.putInt(KEY_ID, id) }

      val intent = Intent(context, AddUpdateProductActivity::class.java)
      intent.putExtras(bundle)
      context.startActivity(intent)
    }
  }

  /** Inject Validation Object */
  @Inject lateinit var validation: FormValidation
  /** Binding Instance */
  private lateinit var binding: ActivityAddUpdateProductBinding
  /** Factory of ViewModel */
  private lateinit var viewModelFactory: ViewModelFactory
  /** Product ViewModel - Handles View state and Product entity related methods */
  private lateinit var productViewModel: ProductViewModel

  /**
   * Entry point of Activity
   */
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    binding = DataBindingUtil.setContentView(this, R.layout.activity_add_update_product)

    setSupportActionBar(binding.toolbar.toolbarMain)

    init()
  }

  /** Instance Variables */
  private var product: Product? = null
  private val id by lazy { intent?.extras?.getInt(KEY_ID) }

  /**
   * It handles variable initialization, Setting click listeners and attaching view model to Activity
   */
  private fun init() {
    // Inject dependencies using Dagger
    ArchitectureApp.instance.component.inject(this)

    viewModelFactory = Injection.provideViewModelFactory(this)

    // Initialize Product View Model
    productViewModel = ViewModelProviders.of(this, viewModelFactory).get(
        ProductViewModel::class.java)

    if (id != null && id!! > 0) {
      refreshUIAsPerUpdateType(UPDATE)

      // Observe changes in Product
      productViewModel.getProduct(id.toString()).observe(this,
          Observer { task -> populateTask(task) })
    } else {
      refreshUIAsPerUpdateType(ADD)
      populateSpinner()
    }

    // Attach onBackPress event with Back button on Toolbar
    binding.toolbar.ivToolbarBack.setOnClickListener { finish() }

    // Add or Update click listener
    binding.buttonAddUpdateProduct.setOnClickListener { _ -> saveData() }
  }

  /**
   * Refresh UI based on Screen event is Update or Add new product
   * It also set Title and Button texts
   */
  private fun refreshUIAsPerUpdateType(type: UPDATE_TYPE) {
    var title: String? = null
    var submitButtonText: String? = null
    when (type) {
      ADD -> {
        title = "Add Product"
        submitButtonText = "Submit"
      }
      UPDATE -> {
        title = "Update Product"
        submitButtonText = "Update"
      }
    }

    binding.toolbar.tvToolbarTitle.text = title
    binding.buttonAddUpdateProduct.text = submitButtonText
  }

  /**
   * Validate form -
   *   If valid proceed to Save Product data
   *   else show error messages over form
   *
   * It is also setting some dummy data of Color and Product Store
   * finish Activity if Product saved successfully
   */
  private fun saveData() {
    if (validation.validateProductForm(binding)) {
      val productId = binding.etProductId.text.toString().Ex_toInt()
      val productName = binding.etProductName.text.toString()
      val productDesc = binding.etProductDesc.text.toString()
      val productRegularprice = binding.etProductRegularPrice.text.toString().Ex_toInt()
      val productSalePrice = binding.etProductSalePrice.text.toString().Ex_toInt()
      productImage = binding.spinnerItemImage.selectedItem.toString()

      val storeId = binding.etStoreID.text.toString().Ex_toInt()
      val storeName = binding.etStoreName.text.toString()
      val storeLocation = binding.etStoreLocation.text.toString()
      val storeContact = binding.etStoreContact.text.toString()

      val storeInfo = Store(storeId = storeId, storeName = storeName, location = storeLocation,
          contact = storeContact)
      val storeMap = HashMap<String, Store>()
      storeMap.put("$storeId", storeInfo)

      val arrayOfStore = ArrayList<HashMap<String, Store>>()
      arrayOfStore.add(storeMap)

      val colors = ArrayList<String>()
      colors.add("#3452DF")
      colors.add("#FFFFFF")
      colors.add("#AC867E")

      val product = Product()
      product.id = productId
      product.name = productName
      product.description = productDesc
      product.regular_price = productRegularprice
      product.sale_price = productSalePrice
      product.product_photo = productImage
      product.colors = colors
      product.stores = arrayOfStore

      productViewModel.saveProduct(product)

      finish()
    }
  }

  /**
   * Populate form with Product data if Product data is available
   */
  private fun populateTask(product: Product?) {
    this.product = product

    binding.etProductId.setText(product?.id.toString())
    binding.etProductId.isEnabled = false
    binding.etProductName.setText(product?.name)
    binding.etProductDesc.setText(product?.description)
    binding.etProductRegularPrice.setText(product?.regular_price.toString())
    binding.etProductSalePrice.setText(product?.sale_price.toString())

    val anyStoreInfo = Store(Random().nextInt(), "Store "+ Random().nextInt(), "Georgia", "+1 888-878-1488")
    var store: Store? = null
    for (index in product?.stores?.indices ?: 0..0) {
      for (storeMap in product?.stores?.get(index)?.keys ?: 0..0) {
        store = product?.stores?.get(index)?.get(storeMap) ?: anyStoreInfo
      }
    }

    binding.etStoreID.setText(store?.storeId.toString())
    binding.etStoreID.isEnabled = false
    binding.etStoreName.setText(store?.storeName)
    binding.etStoreLocation.setText(store?.location)
    binding.etStoreContact.setText(store?.contact)

    productImage = product?.product_photo

    // Populating Spinner of Product Images
    populateSpinner(productImage)
  }

  /**
   * Populate spinner with Image URLs so user can select image of Product
   */
  private fun populateSpinner(selectedImage: String? = null) {

    val optionImage = "https://images-na.ssl-images-amazon.com/images/I/81qonnN9cmL._SL1500_.jpg"
    val image2 = "https://images-na.ssl-images-amazon.com/images/I/31NLyQc8OhL.jpg"
    val image3 = "https://images-na.ssl-images-amazon.com/images/I/51qibZNVexL._SL1050_.jpg"
    val image4 = "https://images-na.ssl-images-amazon.com/images/I/81GrO6PHFjL._SL1500_.jpg"

    val image1 = selectedImage ?: optionImage

    val orderTypes = arrayOf(image1, image2, image3, image4)

    //Creating the ArrayAdapter instance having the Order Types
    val adapterSpinnerOrderType = ArrayAdapter(this, layout.simple_spinner_item, orderTypes)
    adapterSpinnerOrderType.setDropDownViewResource(android.R.layout.simple_spinner_dropdown_item)

    //Setting the ArrayAdapter data on the Spinner
    binding.spinnerItemImage.setAdapter(adapterSpinnerOrderType)

    binding.spinnerItemImage.setOnItemSelectedListener(this)

    loadSelectedImage(image1)
  }

  /**
   * Set Image based on Selected spinner
   */
  private fun loadSelectedImage(imageUrl: String?) {
    imageUrl?.let {
      Glide.with(this).load(imageUrl).into(binding.ivSelectedImage)
    }
  }

  override fun onNothingSelected(p0: AdapterView<*>?) {
  }

  private var itemChangeCount = 0
  private var productImage: String? = null

  /**
   * Select Product Images Listener
   * Set Image based on Selected spinner
   */
  override fun onItemSelected(arg0: AdapterView<*>?, view: View?, position: Int, id: Long) {
    if (itemChangeCount > 0) {
      productImage = binding.spinnerItemImage.selectedItem.toString()
      loadSelectedImage(productImage)
    }

    ++itemChangeCount
  }

}
