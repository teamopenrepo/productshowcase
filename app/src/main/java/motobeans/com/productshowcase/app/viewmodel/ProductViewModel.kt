/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.app.viewmodel

import android.arch.lifecycle.LiveData
import android.arch.lifecycle.MutableLiveData
import android.arch.lifecycle.ViewModel
import android.os.AsyncTask
import io.reactivex.android.schedulers.AndroidSchedulers
import io.reactivex.schedulers.Schedulers
import motobeans.com.productshowcase.application.ArchitectureApp
import motobeans.com.productshowcase.development.interfaces.ApiProject
import motobeans.com.productshowcase.retrofit.response.Response.ResponseProduct
import motobeans.com.productshowcase.app.persistence.dao.ProductDao
import motobeans.com.productshowcase.app.persistence.model.Product
import javax.inject.Inject

/**
 * Created by munishkumarthakur on 09/12/17.
 */
class ProductViewModel(private val productDao: ProductDao) : ViewModel() {

  /** injected variable of network call */
  @Inject lateinit var api: ApiProject

  /** ProductViewState Mutable Live Data */
  val viewState: MutableLiveData<ProductViewState> = MutableLiveData()

  /**
   * Constructor of ProductViewModel
   */
  init {
    ArchitectureApp.instance.component.inject(this)
    viewState.value = ProductViewState()
  }


  /**
   * Get Current View State
   *
   * @return LiveData of View Model State
   */
  private fun currentViewState(): ProductViewState = viewState.value!!

  /** LiveData Variable of List of Product */
  val productList: LiveData<List<Product>?> by lazy {
    productDao.getAllProducts()
  }

  /** LiveData Variable of Int (Shows Record count of Product Table) */
  val productRecordCount: LiveData<Int> by lazy {
    productDao.getProductCount()
  }

  /**
   * Get Product having id = ?
   *
   * @param id Product id
   * @return Product
   */
  fun getProduct(id: String) = productDao.getProductbyId(id)

  /**
   * Save Product
   * Operation Type: Asynchronous
   *
   * @param product Product Entity
   */
  fun saveProduct(product: Product?) {
    AsyncTask.execute({ product?.let { productDao.insertProduct(product) }})
  }

  /**
   * Save List of Products
   * Operation Type: Asynchronous
   *
   * @param products ArrayList of Product Entity
   */
  fun saveProducts(products: ArrayList<Product>?) {
    AsyncTask.execute({ products?.let { productDao.insertProducts(products) }})
  }

  /**
   * Delete product having id = ?
   * Operation Type: Asynchronous
   *
   * @param id product id
   */
  fun deleteProductById(id: Int) {
    AsyncTask.execute({ productDao.deleteProductById(id) })
  }

  /**
   * This class contains different states of screen such as:
   *
   *   isLoading = api call is running and getting data from network
   *
   *   isError = if any error occurs during network operation
   *
   *   isEmptyData = if no data is available
   */
  data class ProductViewState(var isNoDataFound: Boolean = false,
      val isLoading: Boolean = false,
      val isError: Boolean = false,
      val isEmptyData: Boolean = false,
      val errorMessage: String? = null)

  /**
   * Set loading view state
   */
  protected fun isLoading(isLoading: Boolean){
    viewState.value = currentViewState().copy(isLoading = isLoading)
  }

  /**
   * Set Error view state
   */
  protected fun isError(isError: Boolean, errorMessage: String?){
    viewState.value = currentViewState().copy(isError = isError, errorMessage = errorMessage)
  }

  /**
   * Set Empty view state
   */
  protected fun isEmptyList(isEmptyData: Boolean){
    viewState.value = currentViewState().copy(isEmptyData = isEmptyData)
  }

  /**
   * Call network api to fetch product list
   * Operation Type: Asynchronous
   */
  fun fetchDataFromApi() {
    val requestApi = api.api.getProductList()

    requestApi
        .subscribeOn(Schedulers.io())
        .observeOn(AndroidSchedulers.mainThread())
        .doOnSubscribe { _ -> isLoading(true) }
        .doFinally { isLoading(false) }
        .subscribe({ resposne -> processFetchData(resposne) }, { e ->
          e.printStackTrace()
          isError(true, e.message) })
  }

  /**
   * Process product list data
   */
  fun processFetchData(response : ResponseProduct){
    if (response.isSucess) {
      saveProducts(response.product)
    }
  }

}