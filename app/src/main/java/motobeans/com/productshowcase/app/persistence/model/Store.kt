/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.app.persistence.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

/**
* Entity of Store table with
* @param tableName store
*/
@Entity(tableName = "store")
data class Store (

    /** The unique ID of the store. */
    @PrimaryKey
    val storeId: Int,

    /** Name of the store. */
    val storeName: String,

    /** Location of the store. */
    var location: String,

    /** Contact detail of the store. */
    var contact: String
)
