/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.app.persistence.model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import motobeans.com.productshowcase.app.persistence.converters.ConverterArrayListHashMap
import motobeans.com.productshowcase.app.persistence.converters.ConverterArrayList


/**
 * Entity of Product table with
 * @param tableName product
 */
@Entity(tableName = "product")
class Product {

    /** The unique ID of the product. */
    @PrimaryKey
    var id: Int? = 0

    /** Name of the product. */
    var name: String? = ""

    /** Description of the product. */
    var description: String? = ""

    /** Regular price of the product. */
    var regular_price: Int? = 0

    /** Sale Price of the product. */
    var sale_price: Int? = 0

    /** Photo of the product. */
    var product_photo: String? = ""

    /** Color Array of the product. */
    @TypeConverters(ConverterArrayList::class)
    var colors: ArrayList<String>? = ArrayList()

    /** Stores Array of the product. */
    @TypeConverters(ConverterArrayListHashMap::class)
    var stores: ArrayList<HashMap<String, Store>> = ArrayList()

}
