/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.app.view.activity

import android.arch.lifecycle.Observer
import android.arch.lifecycle.ViewModelProviders
import android.os.Bundle
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.LinearLayoutManager
import android.support.v7.widget.RecyclerView.LayoutManager
import android.view.View
import kotlinx.android.synthetic.main.activity_product_list.fab
import kotlinx.android.synthetic.main.activity_product_list.toolbar
import kotlinx.android.synthetic.main.content_product_list.pbLoading
import kotlinx.android.synthetic.main.content_product_list.rvProducts
import motobeans.com.productshowcase.R
import motobeans.com.productshowcase.app.viewmodel.Injection
import motobeans.com.productshowcase.app.viewmodel.ProductViewModel
import motobeans.com.productshowcase.app.viewmodel.ProductViewModel.ProductViewState
import motobeans.com.productshowcase.app.persistence.model.Product
import motobeans.com.productshowcase.app.view.adapters.recycler.ProductItemRecycler

/**
 * Created by munishkumarthakur on 09/12/17.
 */
class ProductListActivity : AppCompatActivity() {

  /**
   * Entry point of Activity
   */
  override fun onCreate(savedInstanceState: Bundle?) {
    super.onCreate(savedInstanceState)
    setContentView(R.layout.activity_product_list)

    setSupportActionBar(toolbar)

    init()
  }

  private lateinit var productViewModel: ProductViewModel
  var products: List<Product>? = null

  /**
   * It handles variable initialization, Setting click listeners and attaching view model to Activity
   */
  private fun init() {
    val viewModelFactory = Injection.provideViewModelFactory(this)

    // Initialize Product View Model
    productViewModel = ViewModelProviders.of(this, viewModelFactory).get(
        ProductViewModel::class.java)

    // Attach adapter to screen
    populateTaskList()

    // Observe changes in Product List
    productViewModel.productList.observe(this, Observer { products ->
      this.products = products
      populateUI(products)
    })

    // Observer changes whenever count of Product in DB changes
    productViewModel.productRecordCount.observe(this, Observer { count ->
      count?.let { if (count <= 0) productViewModel.fetchDataFromApi() }
    })

    // Observer changes of ViewState in ViewModel
    productViewModel.viewState.observe(this, Observer { viewState -> handleViewState(viewState) })

    // Click floating button listener
    fab.setOnClickListener { _ -> addProduct() }

  }

  /**
   * Handle view state when changed and reflect changes data over screen
   */
  private fun handleViewState(viewState: ProductViewState?) {
    viewState?.isLoading?.let {
      if(viewState.isLoading) pbLoading.visibility = View.VISIBLE
      else pbLoading.visibility = View.GONE
    }
  }

  /**
   * Set count of Product List items on title of Screen
   */
  private fun populateUI(products: List<Product>?) {
    toolbar.title = "Product count: ${products?.size ?: 0}"
  }

  /** LayoutManager for RecyclerView */
  private val recylerViewLayoutManager: LayoutManager = LinearLayoutManager(this)
  /** Adapter variable of Product */
  private lateinit var adapter: ProductItemRecycler

  /**
   * Populate Product list adapter and attach to recycler view
   */
  private fun populateTaskList() {
    adapter = ProductItemRecycler(this, productViewModel)
    rvProducts.layoutManager = recylerViewLayoutManager
    rvProducts.adapter = adapter
    rvProducts.setHasFixedSize(true)
    rvProducts.isNestedScrollingEnabled = false
  }

  /**
   * Start Add Product Activity
   */
  private fun addProduct() {
    AddUpdateProductActivity.start(this)
  }

}
