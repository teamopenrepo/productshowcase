/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.app.view.adapters.holders

import android.content.Context
import android.support.v7.widget.RecyclerView
import motobeans.com.productshowcase.app.persistence.model.Store
import motobeans.com.productshowcase.databinding.ItemStoreBinding

/**
 * RecyclerAdapter's Holder to populate Store view
 * @param context Context
 * @param binding ItemStoreBinding
 */
class StoreHolder(val context: Context, val binding: ItemStoreBinding) : RecyclerView.ViewHolder(binding.root) {

  /**
   * Handles view of Store Item
   * It accepts Store Entity and attach to the view
   *
   * @param position Int (position of view in RecyclerView)
   * @param itemData Store
   */
  fun handleCard(position: Int, itemData: Store) {
    binding.tvStoreName.text = itemData.storeName
    binding.tvStoreLocation.text = itemData.location
    binding.tvStoreContact.text = itemData.contact
  }
}