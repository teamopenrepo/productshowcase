/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.app.view.adapters.recycler

import android.arch.lifecycle.Observer
import android.content.Context
import android.databinding.DataBindingUtil
import android.support.v7.app.AppCompatActivity
import android.support.v7.widget.RecyclerView
import android.view.LayoutInflater
import android.view.ViewGroup
import motobeans.com.productshowcase.app.view.adapters.holders.ProductItemHolder
import motobeans.com.productshowcase.R
import motobeans.com.productshowcase.app.viewmodel.ProductViewModel
import motobeans.com.productshowcase.databinding.ItemProductBinding

/**
 * RecyclerAdapter to populate Products
 * @param activity AppCompatActivity
 * @param productViewModel ProductViewModel - This help to update recyclerview itself if any DB changes occur
 */
class ProductItemRecycler(activity: AppCompatActivity, val productViewModel: ProductViewModel) : RecyclerView.Adapter<ProductItemHolder>() {
  val context: Context = activity

  /**
   * Initialize Product ViewModel related identities
   */
  init {
    // Observer changes in Product List and Notify it happens
    productViewModel.productList.observe(activity, Observer { notifyDataSetChanged() })
  }

  /**
   * Get Layout Inflator
   */
  private fun getLayoutInflater(parent: ViewGroup?): LayoutInflater {
    return LayoutInflater.from(parent?.context)
  }

  override fun onCreateViewHolder(parent: ViewGroup?, viewType: Int): ProductItemHolder {
    val binding: ItemProductBinding = DataBindingUtil.inflate(getLayoutInflater(parent), R.layout.item_product, parent, false)
    return ProductItemHolder(context = context, binding = binding)
  }

  override fun getItemCount(): Int {
    return productViewModel.productList.value?.size ?: 0
  }

  override fun onBindViewHolder(holder: ProductItemHolder?, position: Int) {
    holder?.handleCard(position, productViewModel.productList.value!!.get(position))
  }
}