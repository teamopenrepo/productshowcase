/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.constants

/**
 * Constants of Application
 */
class Constants {

  private fun Constants(): Unit {}

  /**
   * Dagger Constants
   */
  object Injection {
    const val API_CURRENT_URL = "currentURL"

    const val API_DEVELOPMENT_URL = "developmentURL"
    const val API_TESTING_URL = "testingURL"
    const val API_LIVE_URL = "liveURL"
    const val API_PRODUCTION_URL = "productionURL"


    /**
     * Network Class v1 constants
     */
    const val RETROFIT_V1 = "RETROFIT_V1"
    const val GSON_V1 = "GSON_V1"
    const val ENDPOINT_V1 = "GSON_V1"
    const val OKHHTP_CACHE_V1 = "OKHHTP_CACHE_V1"
    const val OKHHTP_CLIENT_V1 = "OKHHTP_CLIENT_V1"
    const val INTERCEPTOR_HEADER_V1 = "INTERCEPTOR_HEADER_V1"
    const val INTERCEPTOR_LOGGING_V1 = "INTERCEPTOR_LOGGING_V1"
    const val INTERCEPTOR_CACHING_V1 = "INTERCEPTOR_CACHING_V1"
  }

  /**
   * Api Constants
   */
  object API {

    /**
     * URL of Api Constants
     */
    object URL {

      const val URL_DEVELOPMENT = "https://api.myjson.com/"
      const val URL_TESTING = ""
      const val URL_LIVE = ""
      const val URL_PRODUCTION = ""
    }
  }
}