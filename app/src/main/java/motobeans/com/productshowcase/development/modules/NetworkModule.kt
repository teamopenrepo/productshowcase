/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.development.modules

import android.app.Application
import com.google.gson.Gson
import com.google.gson.GsonBuilder
import dagger.Module
import dagger.Provides
import motobeans.com.productshowcase.constants.Constants
import motobeans.com.productshowcase.development.implementation.ApiProjectImpl
import motobeans.com.productshowcase.development.interfaces.ApiProject
import motobeans.com.productshowcase.development.interfaces.EndPoint
import motobeans.com.productshowcase.constants.Constants.Injection.ENDPOINT_V1
import motobeans.com.productshowcase.constants.Constants.Injection.GSON_V1
import motobeans.com.productshowcase.constants.Constants.Injection.INTERCEPTOR_HEADER_V1
import motobeans.com.productshowcase.constants.Constants.Injection.INTERCEPTOR_LOGGING_V1
import motobeans.com.productshowcase.constants.Constants.Injection.OKHHTP_CACHE_V1
import motobeans.com.productshowcase.constants.Constants.Injection.OKHHTP_CLIENT_V1
import motobeans.com.productshowcase.constants.Constants.Injection.RETROFIT_V1
import motobeans.com.productshowcase.development.implementation.ApiEndPointImpl
import okhttp3.Cache
import okhttp3.Interceptor
import okhttp3.OkHttpClient
import okhttp3.logging.HttpLoggingInterceptor
import retrofit2.Retrofit
import retrofit2.adapter.rxjava2.RxJava2CallAdapterFactory
import retrofit2.converter.gson.GsonConverterFactory
import java.io.File
import java.util.concurrent.TimeUnit
import javax.inject.Named
import javax.inject.Singleton

/**
 * Dagger Module which provides Network Module
 */
@Module
class NetworkModule {

  /**
   * Provides Singleton Gson object
   * @return Gson
   *
   * @see Gson
   * @see Provides
   * @see Singleton
   */
  @Provides
  @Singleton
  @Named(GSON_V1)
  internal fun provideGsonV1(): Gson {
    val gsonBuilder = GsonBuilder()
    return gsonBuilder.create()
  }

  /**
   * Provides Singleton Cache Object
   * @param application Application
   * @return Cache
   *
   * @see Application
   * @see Cache
   * @see Provides
   * @see Singleton
   */
  @Provides
  @Singleton
  @Named(OKHHTP_CACHE_V1)
  internal fun provideOkHttpCacheV1(application: Application): Cache {
    // Install an HTTP cache in the application cache directory.
    val cacheDir = File(application.cacheDir, "http")

    return Cache(cacheDir, DISK_CACHE_SIZE.toLong())
  }

  /**
   * Provides Singleton Retrofit Object
   * @param cache Cache
   * @param logging HttpLoggingInterceptor
   * @param headerInterceptor Interceptor
   * @return OkHttpClient
   *
   * @see Cache
   * @see Interceptor
   * @see HttpLoggingInterceptor
   * @see Provides
   * @see Singleton
   */
  @Provides
  @Singleton
  @Named(OKHHTP_CLIENT_V1)
  internal fun provideOkHttpClientV1(@Named(OKHHTP_CACHE_V1) cache: Cache,
      @Named(INTERCEPTOR_LOGGING_V1) logging: HttpLoggingInterceptor,
      @Named(INTERCEPTOR_HEADER_V1) headerInterceptor: Interceptor): OkHttpClient {
    val builder = OkHttpClient.Builder()
    //builder.networkInterceptors().add(cachingControlInterceptor);

    return builder
        .addInterceptor(headerInterceptor)
        .readTimeout(60, TimeUnit.SECONDS)
        .connectTimeout(60, TimeUnit.SECONDS)
        .addInterceptor(logging)
        .cache(cache)
        .build()
  }

  /**
   * Provides Singleton Interceptor Object
   * @return Interceptor
   *
   * @see Interceptor
   * @see Provides
   * @see Singleton
   */
  @Provides
  @Singleton
  @Named(INTERCEPTOR_HEADER_V1)
  internal fun provideRetrofitHeaderV1(): Interceptor {
    return Interceptor { chain ->

      val original = chain.request()

      val builder = original.newBuilder()
      builder.header("Content-Type", "application/json")
          .method(original.method(), original.body())
      builder.header("platform", "Android")

      val request = builder.build()

      chain.proceed(request)
    }
  }

  /**
   * Provides Singleton HttpLoggingInterceptor Object
   * @return HttpLoggingInterceptor
   *
   * @see HttpLoggingInterceptor
   * @see Provides
   * @see Singleton
   */
  @Provides
  @Singleton
  @Named(INTERCEPTOR_LOGGING_V1)
  internal fun provideLoggingInterceptorV1(): HttpLoggingInterceptor {
    val logging = HttpLoggingInterceptor()
    logging.level = HttpLoggingInterceptor.Level.BODY
    return logging
  }

  /**
   * Provides Singleton EndPoint Object
   * @param String Network URL
   * @return EndPoint
   *
   * @see Provides
   * @see Singleton
   */
  @Provides
  @Named(ENDPOINT_V1)
  internal fun provideEndPointV1(@Named(Constants.Injection.API_CURRENT_URL)
  URL: String): EndPoint {
    return ApiEndPointImpl().setEndPoint(URL)
  }

  /**
   * Provides Singleton Retrofit Object
   * @param gson Gson
   * @param okHttpClient OkHttpClient
   * @param endPoint EndPoint
   * @return Retrofit
   *
   * @see Retrofit
   * @see Gson
   * @see OkHttpClient
   * @see EndPoint
   * @see Provides
   * @see Singleton
   */
  @Provides
  @Singleton
  @Named(RETROFIT_V1)
  internal fun provideRetrofitV1(@Named(GSON_V1) gson: Gson,
      @Named(OKHHTP_CLIENT_V1) okHttpClient: OkHttpClient,
      @Named(ENDPOINT_V1) endPoint: EndPoint): Retrofit {
    return Retrofit.Builder()
        .addConverterFactory(GsonConverterFactory.create(gson))
        .addCallAdapterFactory(RxJava2CallAdapterFactory.create())
        .baseUrl(endPoint.url!!)
        .client(okHttpClient)
        .build()
  }

  /**
   * Provides Singleton Api (Network) object
   * @param retrofit Retrofit
   * @return ApiProject
   *
   * @see Retrofit
   * @see Provides
   * @see Singleton
   */
  @Provides
  @Singleton
  internal fun provideRetrofitApiV1(@Named(RETROFIT_V1) retrofit: Retrofit): ApiProject {
    return ApiProjectImpl(retrofit)
  }

  companion object {
    internal val DISK_CACHE_SIZE = 1024 * 1024
  }

}
