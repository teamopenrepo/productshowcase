/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.development.implementation

import motobeans.com.productshowcase.development.interfaces.EndPoint

/**
 * URL of Network call
 */
class ApiEndPointImpl : EndPoint {

  var sUrl : String? = null

  /** URL of Network */
  override val url: String?
    get() = sUrl

  override val name: String?
    get() = "Project" //To change initializer of created properties use File | Settings | File Templates.

  /**
   * Set Network URL
   *
   * @param ApiEndPointImpl Implementation of
   * @see ApiEndPointImpl
   */
  fun setEndPoint(url: String): ApiEndPointImpl {
    this.sUrl = url
    return this
  }
}