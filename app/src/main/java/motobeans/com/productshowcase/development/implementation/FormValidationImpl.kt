/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.development.implementation

import android.content.Context
import android.support.design.widget.TextInputLayout
import android.widget.EditText
import motobeans.com.productshowcase.databinding.ActivityAddUpdateProductBinding
import motobeans.com.productshowcase.development.interfaces.FormValidation
import motobeans.com.productshowcase.extensions.Ex_isNotEmptyOrNullOrBlank

/**
 * Form Validation which validates different form in Application
 */
class FormValidationImpl(private val context: Context) : FormValidation {

  private val supportTextCantEmpty = " can not be empty"

  /**
   * Validates Product Creation Form
   *
   * @param ActivityAddUpdateProductBinding Binding of Activity layout to Validate
   * @return Boolean
   */
  override fun validateProductForm(binding: ActivityAddUpdateProductBinding): Boolean {
    var errorCount = 0
    errorCount += checkAndSetError(binding.etProductId, binding.tilProductId, getErrorEmptyText("Product ID"))
    errorCount += checkAndSetError(binding.etProductName, binding.tilProductName, getErrorEmptyText("Product Name"))
    errorCount += checkAndSetError(binding.etProductDesc, binding.tilProductDesc, getErrorEmptyText("Product Description"))
    errorCount += checkAndSetError(binding.etProductRegularPrice, binding.tilProductRegularPrice, getErrorEmptyText("Regular Price"))
    errorCount += checkAndSetError(binding.etProductSalePrice, binding.tilProductSalePrice, getErrorEmptyText("Sale Price"))
    errorCount += checkAndSetError(binding.etStoreID, binding.tilStoreID, getErrorEmptyText("Store ID"))
    errorCount += checkAndSetError(binding.etStoreName, binding.tilStoreName, getErrorEmptyText("Store Name"))
    errorCount += checkAndSetError(binding.etStoreLocation, binding.tilStoreLocation, getErrorEmptyText("Location"))
    errorCount += checkAndSetError(binding.etStoreContact, binding.tilStoreContact, getErrorEmptyText("Contact Number"))

    return isValidForm(errorCount)
  }

  /**
   * Internal Method to get Complete Empty Error Message
   *
   * @param value String
   * @return String
   */
  private fun getErrorEmptyText(value: String) : String{
    return value + supportTextCantEmpty
  }

  /**
   * Set Error if Value if not valid
   *
   * @param etToCheck EditText
   * @param tilToSet TextInputLayout
   * @param errorText String
   * @return Int: Error Count
   */
  private fun checkAndSetError(etToCheck: EditText, tilToSet: TextInputLayout,
      errorText: String): Int {
    var errorCount = 0
    val valToCheck = etToCheck.text.toString().trim { it <= ' ' }
    if (valToCheck.Ex_isNotEmptyOrNullOrBlank()) {
      tilToSet.isErrorEnabled = false
    } else {
      errorCount++
      tilToSet.isErrorEnabled = true
      tilToSet.error = errorText
    }

    return errorCount
  }

  /**
   * Internal Method to check if form is Valid or not
   *
   * @param errorCount Int
   * @return Boolean
   */
  private fun isValidForm(errorCount: Int): Boolean {
    return errorCount <= 0
  }
}