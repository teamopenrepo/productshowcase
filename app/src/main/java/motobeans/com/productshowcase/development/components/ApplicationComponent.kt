/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.development.components

import android.app.Application
import dagger.Component
import motobeans.com.productshowcase.app.view.activity.AddUpdateProductActivity
import motobeans.com.productshowcase.app.viewmodel.ProductViewModel
import motobeans.com.productshowcase.development.modules.AppModule
import motobeans.com.productshowcase.development.modules.NetworkModule
import motobeans.com.productshowcase.development.modules.PrimitivesModule
import motobeans.com.productshowcase.development.modules.UtilityModule
import javax.inject.Singleton

/**
 * Dagger Component which helps to inject references in appropriate files
 */
@Singleton
@Component(modules = arrayOf(
  AppModule::class, NetworkModule::class, UtilityModule::class, PrimitivesModule::class
))
interface ApplicationComponent {

  fun inject(app: Application)

  /**
   * Activities
   */
  fun inject(activity: AddUpdateProductActivity)

  /**
   * ViewModels
   */
  fun inject(viewModel: ProductViewModel)


}