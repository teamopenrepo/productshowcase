/*
* Copyright (C) 2017 The Munish/BrighterBrain Open Source Project
*
* Licensed under the Apache License, Version 2.0 (the "License");
* you may not use this file except in compliance with the License.
* You may obtain a copy of the License at
*
*      http://www.apache.org/licenses/LICENSE-2.0
*
* Unless required by applicable law or agreed to in writing, software
* distributed under the License is distributed on an "AS IS" BASIS,
* WITHOUT WARRANTIES OR CONDITIONS OF ANY KIND, either express or implied.
* See the License for the specific language governing permissions and
* limitations under the License.
*/
package motobeans.com.productshowcase.development.modules

import dagger.Module
import dagger.Provides
import motobeans.com.productshowcase.constants.Constants
import javax.inject.Named
import javax.inject.Singleton

/**
 * Dagger Module which provides Primitives Module
 */
@Module
class PrimitivesModule {

  /**
   * Provides Current URL which is set for the application (Developer can make this dynamic via code as well)
   * @return String
   *
   * @see Provides
   */
  @Provides
  @Named(Constants.Injection.API_CURRENT_URL)
  fun provideCurrentURL(@Named(Constants.Injection.API_DEVELOPMENT_URL) URL: String): String {
    return URL
  }

  /**
   * Provides Development URL
   * @return String
   *
   * @see Provides
   */
  @Provides
  @Named(Constants.Injection.API_DEVELOPMENT_URL)
  fun provideDevelopmentURL(): String {
    return Constants.API.URL.URL_DEVELOPMENT
  }

  /**
   * Provides Testing URL
   * @return String
   *
   * @see Provides
   */
  @Provides
  @Named(Constants.Injection.API_TESTING_URL)
  fun provideTestingURL(): String {
    return Constants.API.URL.URL_TESTING
  }

  /**
   * Provides Live URL
   * @return String
   *
   * @see Provides
   */
  @Provides
  @Named(Constants.Injection.API_LIVE_URL)
  fun provideLiveURL(): String {
    return Constants.API.URL.URL_LIVE
  }

  /**
   * Provides Production URL
   * @return String
   *
   * @see Provides
   */
  @Provides
  @Named(Constants.Injection.API_PRODUCTION_URL)
  fun provideProductionURL(): String {
    return Constants.API.URL.URL_PRODUCTION
  }
}
