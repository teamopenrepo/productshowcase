package motobeans.com.productshowcase.app.persistence.DAO;

import android.arch.lifecycle.LiveData;
import android.arch.lifecycle.Observer;
import android.arch.persistence.room.Room;
import android.content.Context;
import android.support.test.InstrumentationRegistry;
import android.support.test.runner.AndroidJUnit4;
import io.reactivex.annotations.Nullable;
import java.util.ArrayList;
import java.util.HashMap;
import java.util.concurrent.CountDownLatch;
import java.util.concurrent.TimeUnit;
import motobeans.com.productshowcase.app.persistence.dao.ProductDao;
import motobeans.com.productshowcase.app.persistence.db.ProductDB;
import motobeans.com.productshowcase.app.persistence.model.Product;
import motobeans.com.productshowcase.app.persistence.model.Store;
import org.junit.After;
import org.junit.Assert;
import org.junit.Before;
import org.junit.Test;
import org.junit.runner.RunWith;

@RunWith(AndroidJUnit4.class)
public class ProductDaoTest {
    private ProductDB productDb;
    private ProductDao productDao;
    private Product product;

    @Before
    public void initDb() {
        Context context = InstrumentationRegistry.getTargetContext();
        productDb = Room.inMemoryDatabaseBuilder(context, ProductDB.class).build();
        productDao = productDb.productDao();
        product = mockProduct();
        productDao.insertProduct(product);
    }

    @After
    public void closeDb() {
        productDb.close();
    }


    @Test
    public void getAllProducts() {
        try {
            Product retreivedProduct = getValue(productDao.getAllProducts()).get(0);
            Assert.assertEquals(product.getId(), retreivedProduct.getId());
            Assert.assertEquals(product.getName(), retreivedProduct.getName());
            Assert.assertEquals(product.getDescription(), retreivedProduct.getDescription());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getProductbyId() {
        try {
            Product retreivedProduct = getValue(productDao.getProductbyId(product.getId().toString()));
            Assert.assertEquals(product.getId(), retreivedProduct.getId());
            Assert.assertEquals(product.getName(), retreivedProduct.getName());
            Assert.assertEquals(product.getDescription(), retreivedProduct.getDescription());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void getProductCount() {
        int retreivedProduct = 0;
        try {
            retreivedProduct = getValue(productDao.getProductCount());
            Assert.assertEquals(1, retreivedProduct);

        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    @Test
    public void deleteProduct() {
        productDao.deleteAllProducts();
        try {
            Assert.assertEquals(0, getValue(productDao.getProductCount()).intValue());
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private Product mockProduct() {
        Product product = new Product();
        product.setId(1);
        product.setDescription("Testing Description for the Test Product1");
        product.setName("Test Product 1");
        product.setRegular_price(1000);
        product.setSale_price(900);
        ArrayList<HashMap<String, Store>> stores = new ArrayList<>();
        Store store = mockStore();
        HashMap<String, Store> map = new HashMap<>();
        map.put(Integer.toString(store.getStoreId()), store);
        stores.add(map);

        product.setStores(stores);
        return product;
    }

    private static Store mockStore() {
        return new Store(1, "Test Store 1", "Delhi", "1111111111");
    }

    public static <T> T getValue(final LiveData<T> liveData) throws InterruptedException {
        final Object[] data = new Object[1];
        final CountDownLatch latch = new CountDownLatch(1);
        Observer<T> observer = new Observer<T>() {
            @Override
            public void onChanged(@Nullable T o) {
                data[0] = o;
                latch.countDown();
                liveData.removeObserver(this);
            }
        };
        liveData.observeForever(observer);
        latch.await(2, TimeUnit.SECONDS);
        //noinspection unchecked
        return (T) data[0];
    }
}
