package motobeans.com.productshowcase.app.persistence.Model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey

@Entity(tableName = "store")
data class Store (
    @PrimaryKey
    val storeId: Int,
    val storeName: String,
    var location: String,
    var contact: String
)
