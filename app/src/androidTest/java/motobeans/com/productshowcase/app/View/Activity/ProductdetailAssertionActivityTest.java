package motobeans.com.productshowcase.app.View.Activity;

import android.support.test.espresso.ViewInteraction;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import motobeans.com.productshowcase.R;
import motobeans.com.productshowcase.app.view.activity.ProductListActivity;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeUp;
import static android.support.test.espresso.assertion.ViewAssertions.matches;
import static android.support.test.espresso.contrib.RecyclerViewActions.actionOnItemAtPosition;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

@LargeTest
@RunWith(AndroidJUnit4.class)
public class ProductdetailAssertionActivityTest {

    @Rule
    public ActivityTestRule<ProductListActivity> mActivityTestRule = new ActivityTestRule<>(ProductListActivity.class);

    @Test
    public void productdetailAssertionActivityTest() {
        ViewInteraction recyclerView = onView(
                allOf(withId(R.id.rvProducts),
                        childAtPosition(
                                withId(R.id.includeProductContent),
                                0)));
        try {
            Thread.sleep(700);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        recyclerView.perform(actionOnItemAtPosition(1, click()));

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(700);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        ViewInteraction imageView = onView((withId(R.id.ivProductImage)));
        imageView.check(matches(isDisplayed()));

        ViewInteraction textView7 = onView(withId(R.id.tvProductName));
        textView7.check(matches(isDisplayed()));

        ViewInteraction linearLayout3 = onView(withId(R.id.llProductBlock));
        linearLayout3.check(matches(isDisplayed()));

        ViewInteraction textView8 = onView((withText("Description")));
        textView8.check(matches(isDisplayed()));

        ViewInteraction textView9 = onView(withId(R.id.tvProductDescription));
        textView9.check(matches(isDisplayed()));

        ViewInteraction textView11 = onView(withId(R.id.tvRegularPrice));
        textView11.check(matches(isDisplayed()));

        ViewInteraction textView12 = onView(withId(R.id.tvSalePrice));
        textView12.check(matches(isDisplayed()));


        ViewInteraction button = onView(withId(R.id.buttonEditProduct));
        button.check(matches(isDisplayed()));

        onView(withId(R.id.nestedScroll)).perform(swipeUp(), click());

        ViewInteraction buttonDeleteProduct = onView(withId(R.id.buttonDeleteProduct));
        buttonDeleteProduct.check(matches(isDisplayed()));

        ViewInteraction linearLayout = onView(withId(R.id.rvProductColor));
        linearLayout.check(matches(isDisplayed()));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
