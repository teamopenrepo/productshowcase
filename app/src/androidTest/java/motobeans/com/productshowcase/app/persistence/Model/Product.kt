package motobeans.com.productshowcase.app.persistence.Model

import android.arch.persistence.room.Entity
import android.arch.persistence.room.PrimaryKey
import android.arch.persistence.room.TypeConverters
import motobeans.com.productshowcase.app.persistence.converters.ConverterArrayList
import motobeans.com.productshowcase.app.persistence.converters.ConverterArrayListHashMap

@Entity(tableName = "product")
class Product {

    @PrimaryKey
    var id: Int? = 0
    var name: String? = ""
    var description: String? = ""
    var regular_price: Int? = 0
    var sale_price: Int? = 0
    var product_photo: String? = ""

    @TypeConverters(ConverterArrayList::class)
    var colors: ArrayList<String>? = ArrayList()

    @TypeConverters(ConverterArrayListHashMap::class)
    var stores: ArrayList<HashMap<String, Store>> = ArrayList()

}
