package motobeans.com.productshowcase.app.View.Activity;

import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import motobeans.com.productshowcase.R;
import motobeans.com.productshowcase.app.view.activity.AddUpdateProductActivity;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.swipeUp;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

/**
 * This test aims at checking if all the views are visible or not
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class AddUpdateProductActivityTest {

    @Rule
    public ActivityTestRule<AddUpdateProductActivity> mActivityTestRule = new ActivityTestRule<>(AddUpdateProductActivity.class);

    @Test
    public void productListActivityTest() {

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(700);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        onView(allOf(withId(R.id.etProductId), childAtPosition(childAtPosition(withId(R.id.tilProductId), 0), 0), isDisplayed()));

        onView(allOf(withId(R.id.etProductName), childAtPosition(childAtPosition(withId(R.id.tilProductName), 0), 0), isDisplayed()));

        onView(allOf(withId(R.id.etProductDesc), childAtPosition(childAtPosition(withId(R.id.tilProductDesc), 0), 0), isDisplayed()));

        onView(allOf(withId(R.id.etProductRegularPrice), childAtPosition(childAtPosition(withId(R.id.tilProductRegularPrice), 0), 0), isDisplayed()));

        onView(allOf(withId(R.id.etProductSalePrice), childAtPosition(childAtPosition(withId(R.id.tilProductSalePrice), 0), 0), isDisplayed()));


        onView(withId(R.id.nestedScroll)).perform(swipeUp(), click());

        onView(allOf(withId(R.id.etStoreID), childAtPosition(childAtPosition(withId(R.id.tilStoreID), 0), 0), isDisplayed()));

        onView(allOf(withId(R.id.etStoreName), childAtPosition(childAtPosition(withId(R.id.tilStoreName), 0), 0), isDisplayed()));

        onView(allOf(withId(R.id.etStoreLocation), childAtPosition(childAtPosition(withId(R.id.tilStoreLocation), 0), 0), isDisplayed()));

        onView(allOf(withId(R.id.etStoreContact), childAtPosition(childAtPosition(withId(R.id.tilStoreContact), 0), 0), isDisplayed()));

        onView(allOf(withId(R.id.etStoreContact), childAtPosition(childAtPosition(withId(R.id.tilStoreContact), 0), 0), isDisplayed()));

        onView(allOf(withId(R.id.buttonAddUpdateProduct), withText("Submit"), childAtPosition(childAtPosition(withId(android.R.id.content), 0), 1), isDisplayed()));
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }
}
