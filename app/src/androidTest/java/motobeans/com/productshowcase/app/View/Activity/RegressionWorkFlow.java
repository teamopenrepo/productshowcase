package motobeans.com.productshowcase.app.View.Activity;

import android.support.test.espresso.ViewInteraction;
import android.support.test.espresso.assertion.ViewAssertions;
import android.support.test.espresso.contrib.RecyclerViewActions;
import android.support.test.espresso.matcher.ViewMatchers;
import android.support.test.rule.ActivityTestRule;
import android.support.test.runner.AndroidJUnit4;
import android.test.suitebuilder.annotation.LargeTest;
import android.view.View;
import android.view.ViewGroup;
import android.view.ViewParent;
import java.util.ArrayList;
import java.util.HashMap;
import motobeans.com.productshowcase.R;
import motobeans.com.productshowcase.app.persistence.model.Product;
import motobeans.com.productshowcase.app.persistence.model.Store;
import motobeans.com.productshowcase.app.view.activity.ProductListActivity;
import org.hamcrest.Description;
import org.hamcrest.Matcher;
import org.hamcrest.TypeSafeMatcher;
import org.junit.Rule;
import org.junit.Test;
import org.junit.runner.RunWith;

import static android.support.test.espresso.Espresso.onView;
import static android.support.test.espresso.action.ViewActions.click;
import static android.support.test.espresso.action.ViewActions.closeSoftKeyboard;
import static android.support.test.espresso.action.ViewActions.replaceText;
import static android.support.test.espresso.action.ViewActions.swipeUp;
import static android.support.test.espresso.matcher.ViewMatchers.isDisplayed;
import static android.support.test.espresso.matcher.ViewMatchers.withId;
import static android.support.test.espresso.matcher.ViewMatchers.withText;
import static org.hamcrest.Matchers.allOf;

/**
 * Complete Iteration from creation to deletion of product
 */
@LargeTest
@RunWith(AndroidJUnit4.class)
public class RegressionWorkFlow {

    @Rule
    public ActivityTestRule<ProductListActivity> mActivityTestRule = new ActivityTestRule<>(ProductListActivity.class);

    @Test
    public void productListActivityTest4() {
        Product product = mockModel();
        ViewInteraction floatingActionButton = onView(allOf(withId(R.id.fab), childAtPosition(childAtPosition(withId(android.R.id.content), 0), 2), isDisplayed()));
        floatingActionButton.perform(click());

        // Added a sleep statement to match the app's execution delay.
        // The recommended way to handle such scenarios is to use Espresso idling resources:
        // https://google.github.io/android-testing-support-library/docs/espresso/idling-resource/index.html
        try {
            Thread.sleep(700);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction appCompatEditText = onView(allOf(withId(R.id.etProductId), childAtPosition(childAtPosition(withId(
            R.id.tilProductId), 0), 0), isDisplayed()));
        appCompatEditText.perform(replaceText(product.getId().toString()), closeSoftKeyboard());

        ViewInteraction appCompatEditText2 = onView(allOf(withId(R.id.etProductName), childAtPosition(childAtPosition(withId(
            R.id.tilProductName), 0), 0), isDisplayed()));
        appCompatEditText2.perform(replaceText(product.getName()), closeSoftKeyboard());

        ViewInteraction appCompatEditText3 = onView(allOf(withId(R.id.etProductDesc), childAtPosition(childAtPosition(withId(
            R.id.tilProductDesc), 0), 0), isDisplayed()));
        appCompatEditText3.perform(replaceText(product.getDescription()), closeSoftKeyboard());

        ViewInteraction appCompatEditText4 = onView(allOf(withId(R.id.etProductRegularPrice), childAtPosition(childAtPosition(withId(
            R.id.tilProductRegularPrice), 0), 0), isDisplayed()));
        appCompatEditText4.perform(replaceText(product.getRegular_price().toString()), closeSoftKeyboard());

        ViewInteraction appCompatEditText5 = onView(allOf(withId(R.id.etProductSalePrice), childAtPosition(childAtPosition(withId(
            R.id.tilProductSalePrice), 0), 0), isDisplayed()));
        appCompatEditText5.perform(replaceText(product.getSale_price().toString()), closeSoftKeyboard());

        onView(withId(R.id.nestedScroll)).perform(swipeUp(), click());

        try {
            Thread.sleep(700);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }

        ViewInteraction appCompatEditText6 = onView(allOf(withId(R.id.etStoreID), childAtPosition(childAtPosition(withId(
            R.id.tilStoreID), 0), 0), isDisplayed()));
        appCompatEditText6.perform(replaceText(product.getStores().get(0).get("1").getStoreId() + ""), closeSoftKeyboard());

        ViewInteraction appCompatEditText7 = onView(allOf(withId(R.id.etStoreName), childAtPosition(childAtPosition(withId(
            R.id.tilStoreName), 0), 0), isDisplayed()));
        appCompatEditText7.perform(replaceText(product.getStores().get(0).get("1").getStoreName()), closeSoftKeyboard());

        ViewInteraction appCompatEditText8 = onView(allOf(withId(R.id.etStoreLocation), childAtPosition(childAtPosition(withId(
            R.id.tilStoreLocation), 0), 0), isDisplayed()));
        appCompatEditText8.perform(replaceText(product.getStores().get(0).get("1").getLocation()), closeSoftKeyboard());

        ViewInteraction appCompatEditText9 = onView(allOf(withId(R.id.etStoreContact), childAtPosition(childAtPosition(withId(
            R.id.tilStoreContact), 0), 0), isDisplayed()));
        appCompatEditText9.perform(replaceText(product.getStores().get(0).get("1").getContact()), closeSoftKeyboard());

        ViewInteraction appCompatButton = onView(allOf(withId(R.id.buttonAddUpdateProduct), withText("Submit"), childAtPosition(childAtPosition(withId(android.R.id.content), 0), 1), isDisplayed()));
        appCompatButton.perform(click());

        onView(allOf(withId(R.id.rvProducts), childAtPosition(withId(R.id.includeProductContent), 0)));


        // checking the newly added item is really the item that we added
        onView(allOf(withText(product.getName()), withId(R.id.tvProductName))).check(ViewAssertions.matches(isDisplayed()));
        onView(allOf(withText(product.getName()), withId(R.id.tvProductDescription))).check(ViewAssertions.doesNotExist());
        onView(allOf(withText(product.getDescription()), withId(R.id.tvProductDescription))).check(ViewAssertions.matches(isDisplayed()));
        onView(allOf(withText(product.getSale_price().toString()), withId(R.id.tvSalePrice))).check(ViewAssertions.matches(isDisplayed()));
        onView(allOf(withText(product.getRegular_price().toString()), withId(R.id.tvRegularPrice))).check(ViewAssertions.matches(isDisplayed()));


        // clicking 0th position
        ViewInteraction recyclerViewItem = onView(ViewMatchers.withId(R.id.rvProducts))
                .perform(RecyclerViewActions.actionOnItemAtPosition(0,
                        click()));

        onView(allOf(withText("Delete"), withId(R.id.buttonDeleteProduct))).check(ViewAssertions.matches(isDisplayed())).perform(click());

        onView(allOf(withId(R.id.rvProducts), childAtPosition(withId(R.id.includeProductContent), 0)));

        try {
            Thread.sleep(700);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
        // checking the item is deleted successfully  or not (Not visible in Recyclerview)
        onView(allOf(withText(product.getName()))).check(ViewAssertions.doesNotExist());
        onView(allOf(withText(product.getName()))).check(ViewAssertions.doesNotExist());
        onView(allOf(withText(product.getDescription()))).check(ViewAssertions.doesNotExist());
        onView(allOf(withText(product.getSale_price().toString()))).check(ViewAssertions.doesNotExist());
        onView(allOf(withText(product.getRegular_price().toString()))).check(ViewAssertions.doesNotExist());
        try {
            Thread.sleep(700);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    private static Matcher<View> childAtPosition(
            final Matcher<View> parentMatcher, final int position) {

        return new TypeSafeMatcher<View>() {
            @Override
            public void describeTo(Description description) {
                description.appendText("Child at position " + position + " in parent ");
                parentMatcher.describeTo(description);
            }

            @Override
            public boolean matchesSafely(View view) {
                ViewParent parent = view.getParent();
                return parent instanceof ViewGroup && parentMatcher.matches(parent)
                        && view.equals(((ViewGroup) parent).getChildAt(position));
            }
        };
    }

    private static Product mockModel() {
        Product product = new Product();
        product.setId(1);
        product.setDescription("Testing Description for the Test Product1");
        product.setName("Test Product 1");
        product.setRegular_price(999999);
        product.setSale_price(900);
        ArrayList<HashMap<String, Store>> stores = new ArrayList<>();
        Store store = mockStore();
        HashMap<String, Store> map = new HashMap<>();
        map.put(Integer.toString(store.getStoreId()), store);
        stores.add(map);

        product.setStores(stores);
        return product;
    }

    private static Store mockStore() {
        return new Store(1, "Test Store 1", "Delhi", "1111111111");
    }
}
